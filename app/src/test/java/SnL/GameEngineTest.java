package SnL;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class GameEngineTest {

  private GameEngine gE = new GameEngine();

  // Überprüft die erwarteten Werte im Board am Anfang des Spiels
  @Test
  public void testBoard() {
    Assert.assertArrayEquals(
        gE.getBoard(),
        new int[] {0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 3, 0, 0, 5, 0, 6, 4, 0, 0, 0, 7});
  }

  // Überprüft, ob sich das Feld nach einem Zug wie erwartet verändert
  @Test
  public void testBoardChange() {
    gE.nextMove(1);
    gE.walk();
    Assert.assertArrayEquals(
        gE.getBoard(),
        new int[] {2, 1, 0, 0, 0, 0, 0, 6, 0, 0, 0, 3, 0, 0, 5, 0, 6, 4, 0, 0, 0, 7});
  }

  // Überprüft, ob sich das Feld nach zwei Zügen wie erwartet verändert
  @Test
  public void testBoardChange2() {
    gE.nextMove(3);
    gE.walk();
    gE.nextMove(4);
    gE.walk();
    Assert.assertArrayEquals(
        gE.getBoard(),
        new int[] {0, 0, 0, 1, 2, 0, 0, 6, 0, 0, 0, 3, 0, 0, 5, 0, 6, 4, 0, 0, 0, 7});
  }

  // Testet, ob sich der Player nach einem Zug ändert
  @Test
  public void testCurrentPlayerChange() {
    gE.setCurrentPlayer(1);
    gE.dice();
    gE.nextMove(4);
    gE.walk();
    Assert.assertEquals(gE.getCurrentPlayer(), 2);
  }

  // Testet, ob sich der Player wieder zurück ändert
  @Test
  public void testCurrentPlayerChange2() {
    gE.setCurrentPlayer(2);
    gE.dice();
    gE.nextMove(4);
    gE.walk();
    Assert.assertEquals(gE.getCurrentPlayer(), 1);
  }

  // Testet das erste Snake Feld
  @Test
  public void testSnake1() {
    gE.setPlayer1Index(11);
    Assert.assertTrue(gE.snake());
  }

  // Testet das zweite Snake Feld
  @Test
  public void testSnake2() {
    gE.setPlayer2Index(17);
    Assert.assertTrue(gE.snake());
  }

  // Testet, ob im folgenden Zug nach Snake der Spieler auf dem erwarteten Feld steht
  @Test
  public void testSnakeNext() {
    gE.setPlayer1Index(17);
    gE.setBoardIndex(17, 1);
    gE.snake();
    Assert.assertEquals(gE.getPlayer1Index(), 1);
  }

  // Testet das Feld Leiter
  @Test
  public void testLadder() {
    gE.setPlayer1Index(14);
    Assert.assertTrue(gE.ladder());
  }

  // Testet, ob im folgenden Zug nach Leiter der Spieler auf dem erwarteten Feld steht
  @Test
  public void testLadderNext() {
    gE.setPlayer1Index(14);
    gE.setBoardIndex(14, 1);
    gE.ladder();
    Assert.assertEquals(gE.getPlayer1Index(), 19);
  }

  // Testet, ob die Methode DiceAgain wirkt
  @Test
  public void testDiceAgainTrue() {
    gE.setPlayer1Index(16);
    Assert.assertTrue(gE.diceAgain());
  }

  // Testet, ob im folgenden Zug nach DiceAgain der Spieler nochmal würfeln darf
  @Test
  public void testDiceAgainNext() {
    gE.setPlayer2Index(5);
    gE.setCurrentPlayer(2);
    gE.dice();
    gE.nextMove(2);
    gE.walk();
    Assert.assertEquals(gE.getCurrentPlayer(), 2);
  }

  // Überprüft ob der Gewinner ausgewertet wird
  @Test
  public void testCheckWinner() {
    gE.setPlayer1Index(21);
    Assert.assertTrue(gE.gameOver());
  }

  // Testet die Variable, die dafür sorgt, dass man nicht nochmal würfeln darf
  @Test
  public void testDiceAvailability() {
    Assert.assertTrue(gE.getDiceAvailable());
  }

  //
  @Test
  public void testDiceUnavailability() {
    gE.nextMove(3);
    Assert.assertFalse(gE.getDiceAvailable());
  }

  // Überprüft, ob Spieler 1 nach dem Würfeln auf dem erwarteten Feld steht
  @Test
  public void testMove() {
    gE.nextMove(3);
    gE.walk();
    Assert.assertEquals(gE.getBoardIndex(3), 1);
  }

  // Überprüft, ob Spieler 2 nach dem Würfeln auf dem erwarteten Feld steht
  @Test
  public void testMove2() {
    gE.setCurrentPlayer(2);
    gE.nextMove(4);
    gE.walk();
    Assert.assertEquals(gE.getBoardIndex(4), 2);
  }

  // Wenn Spieler 1 zu weit würfelt dürfte er sich nicht bewegen dürfen
  @Test
  public void testMoveNotAvailable() {
    gE.setBoardIndex(20, 1);
    gE.nextMove(4);
    gE.walk();
    Assert.assertEquals(gE.getBoardIndex(20), 1);
  }

  // Wenn Spieler 2 zu weit würfelt dürfte er sich nicht bewegen dürfen
  @Test
  public void testMoveNotAvailable2() {
    gE.setBoardIndex(19, 2);
    gE.setCurrentPlayer(2);
    gE.nextMove(3);
    gE.walk();
    Assert.assertEquals(gE.getBoardIndex(19), 2);
  }

  // Testet ob die Würfel Methode eine Zahl > 0 ausgibt
  @Test
  public void testDice() {
    gE.dice();
    Assert.assertNotEquals(gE.getDice(), 0);
  }

  // Überprüft, ob der Output1 richtig ausgegeben wird
  @Test
  public void testOutput11() {
    gE.dice();
    Assert.assertEquals(gE.getOutput1(), "Player 1 rolled " + gE.getDice());
  }

  // Überprüft, ob der Output1 richtig ausgegeben wird
  @Test
  public void testOutput12() {
    gE.nextMove(2);
    gE.walk();
    Assert.assertEquals(gE.getOutput1(), "Player 1 walked " + gE.getDice());
  }

  // Überprüft, ob der Output1 richtig ausgegeben wird
  @Test
  public void testOutput13() {
    gE.setPlayer1Index(20);
    gE.nextMove(3);
    Assert.assertEquals(gE.getOutput1(), "Player 1 can't move");
  }

  // Simuliert ein Spiel, und Testet, ob es zum Ende kommt
  @Test
  public void testSimulation() {
    while (!gE.gameOver()) {
      gE.dice();
      gE.walk();
    }
    Assert.assertTrue(gE.gameOver());
  }
}
