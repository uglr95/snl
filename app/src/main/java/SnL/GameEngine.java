package SnL;

import java.util.Arrays;
import java.util.Random;

public class GameEngine implements GameIF {

  private int[] board;
  private int currentPlayer;
  private final int player1;
  private final int player2;
  private int dice;
  private int player1Index;
  private int player2Index;
  private int player1NextMove;
  private int player2NextMove;
  private int moveCounter;
  private String output1;
  private String output2;
  private boolean diceAvailable;
  boolean printGameOver = false;

  GameEngine() {
    this.currentPlayer = 1;
    this.player1 = 1;
    this.player2 = 2;
    // 0 = leeres Feld
    // 1 = Player 1
    // 2 = Player 2
    // 3 = Rot 1
    // 4 = Rot 2
    // 5 = Grün
    // 6 = Nochmal Würfeln
    // 7 = Ziel
    // 10 = Player 1 next Move
    // 20 = Player 2 next Move
    this.board = new int[] {0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 3, 0, 0, 5, 0, 6, 4, 0, 0, 0, 7};
    this.player1Index = 0;
    this.player2Index = 0;
    this.moveCounter = 0;
    this.dice = 0;
    this.output1 = "";
    this.output2 = "";
    this.diceAvailable = true;
  }

  public int[] getBoard() {
    return board;
  }

  public int getBoardIndex(int index) {
    return this.board[index];
  }

  public void setBoardIndex(int index, int value) {
    this.board[index] = value;
  }

  public int getCurrentPlayer() {
    return currentPlayer;
  }

  public void setCurrentPlayer(int currentPlayer) {
    this.currentPlayer = currentPlayer;
  }

  public int getPlayer1() {
    return player1;
  }

  public int getPlayer2() {
    return player2;
  }

  public int getDice() {
    return dice;
  }

  public int getPlayer1Index() {
    return player1Index;
  }

  public void setPlayer1Index(int player1Index) {
    this.player1Index = player1Index;
  }

  public int getPlayer2Index() {
    return player2Index;
  }

  public void setPlayer2Index(int player2Index) {
    this.player2Index = player2Index;
  }

  public int getPlayer1NextMove() {
    return player1NextMove;
  }

  public int getPlayer2NextMove() {
    return player2NextMove;
  }

  public int getMoveCounter() {
    return moveCounter;
  }

  public void setMoveCounter(int moveCounter) {
    this.moveCounter = moveCounter;
  }

  public String getOutput1() {
    return output1;
  }

  public String getOutput2() {
    return output2;
  }

  public boolean getDiceAvailable() {
    return this.diceAvailable;
  }


  @Override
  public void dice() {
    if (diceAvailable) {
      Random rnd = new Random();
      nextMove(rnd.nextInt(4) + 1);
    }
  }

  @Override
  public void nextMove(int dice) {
    this.dice = dice;

    if (getCurrentPlayer() == 1) {
      if (getPlayer1Index() + getDice() > 21) {
        currentPlayer = 2;
        output1 = "Player 1 can't move";
      } else {
        diceAvailable = false;
        player1NextMove = getPlayer1Index() + getDice();
        setBoardIndex(player1NextMove, 10);
        output1 = "Player 1 rolled " + getDice();
      }

    } else if (getCurrentPlayer() == 2) {
      if (getPlayer2Index() + getDice() > 21) {
        currentPlayer = 1;
        output1 = "Player 2 can't move";
      } else {
        diceAvailable = false;
        player2NextMove = getPlayer2Index() + getDice();
        setBoardIndex(getPlayer2NextMove(), 20);
        output1 = "Player 2 rolled " + getDice();
      }
    }
  }

  @Override
  public void walk() {
    if (!diceAvailable) {
      if (getCurrentPlayer() == 1) {
        if (getPlayer1Index() == getPlayer2Index()) setBoardIndex(getPlayer1Index(), 2);
        else setBoardIndex(getPlayer1Index(), 0);
        player1Index = getPlayer1NextMove();
        setBoardIndex(getPlayer1Index(), 1);
        output1 = "Player 1 walked " + getDice();
        if (diceAgain()) {
          output2 = "Player 1 may roll again";
          currentPlayer = 1;
        } else currentPlayer = 2;
      } else if (getCurrentPlayer() == 2) {
        if (getPlayer2Index() == getPlayer1Index()) setBoardIndex(getPlayer2Index(), 1);
        else setBoardIndex(getPlayer2Index(), 0);
        player2Index = getPlayer2NextMove();
        setBoardIndex(getPlayer2Index(), 2);
        output1 = "Player 2 walked " + getDice();
        if (diceAgain()) {
          output2 = "Player 2 may roll again";
          currentPlayer = 2;
        } else currentPlayer = 1;
      }
      System.out.println(output1);
      System.out.println(Arrays.toString(board));
      gameOver();
      diceAvailable = true;
    }
    System.out.println("Player turn: " + getCurrentPlayer());
  }

  @Override
  public boolean snake() {
    if (currentPlayer == 1) {
      if (getPlayer1Index() == 17) {
        output2 = "Player 1: Snake";
        if (getPlayer1Index() == getPlayer2Index()) setBoardIndex(17, 2);
        else setBoardIndex(17, 4);
        setBoardIndex(1, 1);
        player1Index = 1;
      } else if (getPlayer1Index() == 11) {
        output2 = "Player 1: Snake";
        if (getPlayer1Index() == getPlayer2Index()) setBoardIndex(11, 2);
        else setBoardIndex(11, 3);
        setBoardIndex(6, 1);
        player1Index = 6;
      }
    }
    if (currentPlayer == 2)
      if (getPlayer2Index() == 17) {
        output2 = "Player 2: Snake";
        if (getPlayer1Index() == getPlayer2Index()) setBoardIndex(17, 1);
        else setBoardIndex(17, 4);
        setBoardIndex(1, 2);
        player2Index = 1;
      } else if (getPlayer2Index() == 11) {
        output2 = "Player 2: Snake";
        if (getPlayer1Index() == getPlayer2Index()) setBoardIndex(11, 1);
        else setBoardIndex(11, 3);
        setBoardIndex(6, 2);
        player2Index = 6;
      }
    return true;
  }

  @Override
  public boolean ladder() {
    if (currentPlayer == 1) {
      if (getPlayer1Index() == 14) {
        output2 = "Player 1: Ladder";
        setBoardIndex(14, 5);
        setBoardIndex(19, 1);
        player1Index = 19;
      }
    } else if (currentPlayer == 2) {
      if (getPlayer2Index() == 14) {
        output2 = "Player 2: Ladder";
        setBoardIndex(14, 5);
        setBoardIndex(19, 2);
        player2Index = 19;
      }
    }
    return true;
  }

  @Override
  public boolean diceAgain() {
    if (currentPlayer == 1)
      if (getPlayer1Index() == 16 || getPlayer1Index() == 7) {
        setBoardIndex(7, 6);
        setBoardIndex(16, 6);
        return true;
      }
    if (currentPlayer == 2)
      if (getPlayer2Index() == 16 || getPlayer2Index() == 7) {
        setBoardIndex(7, 6);
        setBoardIndex(16, 6);
        return true;
      }
    return false;
  }

  @Override
  public boolean gameOver() {
    if (getPlayer1Index() == 21 || getPlayer2Index() == 21) {
      if (!printGameOver) {
        String printWinner = (getCurrentPlayer() == 1) ? "2" : "1";
        System.out.println("Player " + printWinner + " Wins!");
        printGameOver = true;
      }
      return true;
    }
    return false;
  }
}
