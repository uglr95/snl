package SnL;

import processing.core.PApplet;
import processing.core.PImage;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class DI extends PApplet {
  public static void main(String[] args) {
    PApplet.runSketch(new String[] {"S&L"}, new DI());
  }

  GameEngine gE = new GameEngine();

    /*
      Darstellung des Spielfelds mit 2D-Array, gefüllt mit den Koordinaten der einzelnen Felder
    */
    int[][] fieldCoordinates =  {{50, 100}, {300, 75}, {400, 75}, {500, 75}, {600, 75}, {700, 75}, {800, 75}, {900, 75},   // Erste Zeile
                                {900, 175}, {900, 275}, {900, 375},                                                        // Erste Spalte
                                {800, 375}, {700, 375}, {600, 375}, {500, 375}, {400, 375}, {300, 375},                    // Zweite Zeile
                                {300, 225}, {400, 225}, {500, 225}, {600, 225}, {725, 225}};                               // Dritte Zeile

  public void settings() {
    size(1000, 500);
  }

  public void setup() {
    rectMode(CENTER);
    ellipseMode(CENTER);
    noStroke();
  }

  /*
    Buttons für den Würfel, Restart und Exit
    Bewegen der Spieler über dem Feld
   */
  public void mousePressed() {
    if (mousePressed) {
      if (mouseX > 75 && mouseX < 175 && mouseY > 280 && mouseY < 310) {
        if (!gE.getDiceAvailable()) {
          fill(255, 0, 0);
          rect(125, 295, 100, 40, 10);
        }
        fill(0);
        rect(125, 400, 70, 70, 20);
        gE.dice();
      } else if (mouseX > 275 && mouseX < 925 && mouseY > 50 && mouseY < 400) {
        gE.walk();
        gE.setMoveCounter(gE.getMoveCounter() + 1);
      }
      if (mouseX > 930 && mouseY > 430 && gE.gameOver()) {
        System.out.println("Restart");
        newGame();
      }
      if (mouseX < 65 && mouseY > 430 && gE.gameOver()) {
        System.out.println("Exit");
        exit();
      }
    }
  }

  /*
    Methode zur Darstellung der Spieler/des nächsten Zugs. Übertragt die Werte aus dem Array in der AL auf das Spielfeld.
   */
  public void updateField() {
    AtomicInteger i = new AtomicInteger();
    Arrays.stream(fieldCoordinates)
        .forEach(
            x -> {
              if (gE.getBoardIndex(i.get()) == 0 && gE.getMoveCounter() > 1) {
                fill(255);
                ellipse(x[0], x[1], 27, 27);
              }
              if (gE.getBoardIndex(i.get()) == 1) {
                fill(255, 165, 0);
                ellipse(x[0], x[1], 27, 27);
              }
              if (gE.getBoardIndex(i.get()) == 2) {
                fill(0, 0, 255);
                ellipse(x[0], x[1], 27, 27);
              }

              if (gE.getBoardIndex(i.get()) == 10) {
                fill(255, 210, 150);
                triangle(x[0], x[1] - 25, x[0] - 15, x[1] - 50, x[0] + 15, x[1] - 50);
              }
              if (gE.getBoardIndex(i.get()) == 20) {
                fill(150, 150, 255);
                triangle(x[0], x[1] - 25, x[0] - 15, x[1] - 50, x[0] + 15, x[1] - 50);
              }
              if (gE.getMoveCounter() >= 1) {
                fill(255, 210, 150);
                ellipse(50, 50, 27, 27);
              }
              if (gE.getMoveCounter() >= 2) {
                fill(150, 150, 255);
                ellipse(50, 100, 27, 27);
              }
              i.getAndIncrement();
            });
  }

  /*
    Methode, die im Fall, dass die Spieler sich auf dem gleichen Feld befinden, die Darstellung für das Feld anpasst.
   */
  public void checkSameField() {
    if (gE.getMoveCounter() >= 1) {
      if (gE.getPlayer1Index() == gE.getPlayer2Index()) {
        fill(255, 165, 0);
        ellipse(fieldCoordinates[gE.getPlayer1Index()][0] + 4, fieldCoordinates[gE.getPlayer1Index()][1] + 4, 27, 27);
        fill(0, 0, 255);
        ellipse(fieldCoordinates[gE.getPlayer2Index()][0] - 4, fieldCoordinates[gE.getPlayer2Index()][1] - 4, 27, 27);
      }
    }
  }

  /*
    Setzt alles auf den Ursprungszustand zurück
   */
  public void newGame() {
    GameEngine gE1 = new GameEngine();
    gE = gE1;
  }

  /*
    Gibt den Gewinner aus und fügt den Restart und Exit Button hinzu
   */
  public void gameOver() {
    if (gE.gameOver()) {
      if (gE.getCurrentPlayer() == 2) {
        fill(255, 210, 150);
        rect(725, 225, 90, 90, 30);
        fill(255, 165, 0);
        ellipse(725, 225, 27, 27);
        text("Player " + gE.getPlayer1() + " wins!", 325, 460);
      } else if (gE.getCurrentPlayer() == 1) {
        fill(150, 150, 255);
        rect(725, 225, 90, 90, 30);
        fill(0, 0, 255);
        ellipse(725, 225, 27, 27);
        text("Player " + gE.getPlayer2() + " wins!", 325, 460);
      }
      textSize(15);
      fill(255);
      text("Restart", 920, 470);
      text("Exit", 30, 470);
    }
  }

  /*
    Erstellen der einzelnen Spielfelder und Elemente
   */
  public void drawBoard() {
    // Player Houses
    fill(255, 210, 150);
    rect(50, 50, 50, 50, 5);
    fill(150, 150, 255);
    rect(50, 100, 50, 50, 5);
    // Lines
    fill(255);
    ellipse(250, 75, 15, 10);
    rect(570, 75, 640, 10);
    rect(900, 225, 10, 300);
    rect(600, 375, 600, 10);
    rect(500, 225, 400, 10);
    rect(300, 300, 10, 100);

    // First Row
    for (int i = 300; i <= 900; i = i + 100) rect(i, 75, 50, 50, 10);
    // First Column
    for (int j = 175; j <= 400; j = j + 100) rect(900, j, 50, 50, 10);
    // Second Row
    for (int k = 300; k <= 900; k = k + 100) rect(k, 375, 50, 50, 10);
    // Second Column
    rect(300, 225, 50, 50, 10);
    // Third Row
    for (int l = 400; l <= 600; l = l + 100) rect(l, 225, 50, 50, 10);
    // Last Field
    rect(725, 225, 90, 90, 30);

    // Feldnummern
    AtomicInteger counter = new AtomicInteger();
    Arrays.stream(fieldCoordinates)
        .forEach(
            x -> {
              if (counter.get() > 0 && counter.get() < 21) {
                fill(0);
                textSize(11);
                text(counter.get(), x[0] - 20, x[1] + 20);
              }
              if (counter.get() == 21) {
                fill(205, 127, 50);
                textSize(20);
                text("Goal", x[0] - 20, x[1] + 5);
              }
              if (counter.get() == 17 || counter.get() == 11) {
                fill(255, 0, 0);
                text(counter.get(), x[0] - 20, x[1] + 20);
              }
              if (counter.get() == 14) {
                fill(0, 200, 0);
                text(counter.get(), x[0] - 20, x[1] + 20);
              }
              if (counter.get() == 7 || counter.get() == 16) {
                fill(255, 0, 255);
                text("2x", x[0] + 10, x[1] - 15);
                text(counter.get(), x[0] - 20, x[1] + 20);
              }
              counter.getAndIncrement();
            });

    // Snakes
    fill(255, 0, 0);
    rect(300, 150, 2, 105);
    rect(800, 225, 2, 255);

    // Ladder
    fill(0, 255, 0);
    rect(500, 300, 2, 105);

    // Elements
    fill(210);
    textSize(15);
    rect(125, 185, 205, 35, 5);
    rect(125, 235, 205, 35, 5);

    fill(0);
    if (gE.getDice() > 0) text(gE.getOutput1(), 60, 190);
    text(gE.getOutput2(), 50, 240);
  }

  /*
    Zeichnet den Würfel und den Button zum Würfel und füllt ihn mit dem zufällig erstellten Wert aus der AL
   */
  public void drawDice() {
    fill(150);
    if (gE.getCurrentPlayer() == 1) fill(255, 210, 150);
    if (gE.getCurrentPlayer() == 2) fill(150, 150, 255);
    rect(125, 295, 105, 45, 10);
    fill(220);
    if (mousePressed && mouseX > 75 && mouseX < 175 && mouseY > 280 && mouseY < 310)
      rect(125, 295, 100, 40, 10);
    fill(0);
    textSize(12);
    text("Roll the Dice", 90, 300);
    textSize(55);
    fill(220);
    if (gE.getMoveCounter() > 0) {
      if (gE.getCurrentPlayer() == 1) fill(150, 150, 255);
      if (gE.getCurrentPlayer() == 2) fill(255, 210, 150);
    }
    rect(125, 400, 70, 70, 20);
    fill(0);
    text(gE.getDice(), 107, 420);
  }

  /*
    Darstellung der beiden Spieler
   */
  public void drawPlayer() {
    fill(255, 165, 0); // Player 1 ROT
    ellipse(50, 50, 27, 27);
    if (gE.getCurrentPlayer() == 1 && gE.getPlayer1Index() == 7) ellipse(900, 75, 27, 27);
    if (gE.getCurrentPlayer() == 1 && gE.getPlayer1Index() == 16) ellipse(300, 375, 27, 27);

    fill(0, 0, 255); // Player 2 BLAU
    ellipse(50, 100, 27, 27);
    if (gE.getCurrentPlayer() == 2 && gE.getPlayer2Index() == 7) ellipse(900, 75, 27, 27);
    if (gE.getCurrentPlayer() == 2 && gE.getPlayer2Index() == 16) ellipse(300, 375, 27, 27);
  }

  public void draw() {
    if (mouseX > 75 && mouseX < 175 && mouseY > 280 && mouseY < 310 && gE.getDiceAvailable()) cursor(HAND);
    else cursor(ARROW);

    PImage img;
    img = loadImage("Download.jpg");
    img.resize(1000, 500);
    background(img);

    gE.ladder();
    gE.snake();
    gE.diceAgain();

    drawBoard();
    drawDice();
    drawPlayer();

    updateField();
    checkSameField();
    gameOver();
  }
}
