package SnL;

public interface GameIF {

  /*
    Erstellen einer Zufallszahl und aufrufen von nextMove()
   */
  void dice();

  /*
    Überprüft ob der Zug möglich ist.
    Speichert den Wert des nächsten Feldes, auf dem sich der Spieler nach dem Laufen befindet.
   */
  void nextMove(int dice);

  /*
    Setzt den Spieler auf das Feld von "nextMove()", wenn diceAgain() true ist, darf der Spieler nochmal würfeln.
   */
  void walk();

  /*
    Spieler wird auf das Feld zurückgesetzt, auf die Snake "zeigt"
   */
  boolean snake();

  /*
    Spieler darf vor auf das Feld, auf die Ladder "zeigt"
   */
  boolean ladder();

  /*
    Spieler darf nochmal Würfeln
   */
  boolean diceAgain();

  /*
    Ermittelt einen Gewinner nach jedem Zug
   */
  boolean gameOver();
}
