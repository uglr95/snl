# Kurzbezeichnung für Ihr Spiel (PiS, SoSe 2021)

**Autor:** Ufuk Güler, 5277184

## Kurzbeschreibung (50-150 Wörter)

Mein Projekt „Snakes and Ladders“, inspiriert vom gleichnamigem Brettspiel, ist ein 2-Spieler Brettspiel, bei dem die Spieler abwechselnd
eine Zahl zwischen 1 und 4 Würfeln. Dabei gibt es verschiedene Spezialfelder (Leiter, Schlange, 2x), 
die den Spieler beim fortschreiten behindern, oder unterstützen sollen.

- **Leiter**: Spieler bewegt sich auf das markierte Feld vor.
- **Schlange**: Spieler bewegt sich zurück auf das markierte Feld.
- **2X**: Spieler darf nochmal Würfeln.
- **Ziel**: genau auf dem Feld „Goal“ landen. Würfelt man zu hoch, ist der Zug beendet.

(84 Wörter)

## Screenshot

![Screenshot](Screenshot.png)

## Bedienungshinweise

Durch den Befehl **gradle run** wird das Spiel gestartet.
Durch den Befehl **gradle test** werden die Tests ausgeführt.

Mit dem **„Roll The Dice“** wird unterhalb des Buttons die gewürfelte Zahl erstellt, außerdem erscheint auf Pfeil über dem Feld, auf
den sich der Spieler nach dem Zug befindet. Mit einem **Klick irgendwo auf das Spielfeld** bewegt sich der Spieler dann auf das Feld.
Ist das Spiel entscheiden erscheint in der unteren rechten Ecke ein **„Restart“ Button**, mit dem sich das Spiel zurücksetzen lässt,
und in der unteren linken Ecke ein **„Exit“ Button**, mit dem sich das Spiel beenden, und das Fenster schließen lässt.


## Dateiübersicht und Lines of Code

    > dir /S /B /A-D .
    \README.md
    \Screenshot.png
    \app\build.gradle
    \app\core.jar
    \app\src\main\java\SnL\DI.java
    \app\src\main\java\SnL\GameEngine.java
    \app\src\main\java\SnL\GameIF.java
    \app\src\main\resources\Download.jpg
    \app\src\test\java\SnL\GameEngineTest.java

    -------------------------------------------------------------------------------
    Language                     files          blank        comment           code
    -------------------------------------------------------------------------------
    Java                             3             68             23            468
    -------------------------------------------------------------------------------
    SUM:                             3             68             23            468
    -------------------------------------------------------------------------------

    ALP: 50,4 %
    DI: 49,6%
    Anzahl der Tests: 24







## Verwendete Quellen

* [Spiele-kostenlos-online - Leiterspiel-snakes-and-ladders](https://www.spiele-kostenlos-online.de/brettspiele/wuerfelspiele/leiterspiel-snakes-and-ladders/) (Abruf, 8.6.2021)
* [Processing Reference](https://processing.org/reference/) (Abruf, 10.6.2021)
* Java Der Grundkurs – Michael Kofler
* [JUnit4 Asserts](https://github.com/junit-team/junit4/wiki/Assertions) (Abruf, 19.6.2021)
* [JUnit Tutorial](https://www.javatpoint.com/junit-tutorial) (Abruf, 19.06.2021)
* [Moodle Folien zu Streams](https://moodle.thm.de/pluginfile.php/325977/mod_resource/content/4/02_streams.html) (Und ff. Folien) (Abruf, 27.6.2021)
* [Stackoverflow zu AtmoicInteger](https://stackoverflow.com/questions/50977793/is-use-of-atomicinteger-for-indexing-in-stream-a-legit-way) (Abruf, 27.6.2021)
* [Wallpaper Wood](http://topworldpic.blogspot.com/2011/10/wood-wallpapers.html) (Abruf, 28.6.2021)

Die Quellen dienten dem Zweck der Recherche, bzw der Inspiration (oder zum Downloaden meines Hintergrundbildes) für mein Projekt.
Der gesamte Code aus den Klassen kommt von mir selbst und es wurde keinerlei Code aus den
Quellen kopiert/übernommen, sondern lediglich höchstens auf mein Projekt passend umgeschrieben.

## Anmerkung

Darstellungsfehler: Wenn Spieler nacheinander auf dem gleichen Feld „Leiter“ oder „Schlange“ landen, verschwindet der Player,
der zuerst auf dem Feld landet, bis zum nächsten Zug.